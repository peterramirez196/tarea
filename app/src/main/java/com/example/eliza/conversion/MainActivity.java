package com.example.eliza.conversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button button,button2;
    TextView textView2,textView3;
    EditText editText,editText2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button)findViewById(R.id.button);
        button2 = (Button)findViewById(R.id.button2);
        textView2= (TextView)findViewById(R.id.textView2);
        textView3= (TextView)findViewById(R.id.textView3);
        editText = (EditText) findViewById(R.id.editText);
        editText2= (EditText) findViewById(R.id.editText2);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double d=0;
                double value=Double.parseDouble(editText.getText().toString());
                d=(value-32)/1.8000;

                textView2.setText(String.valueOf(d));
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double d=0;
                double value=Double.parseDouble(editText2.getText().toString());
                d=(value*1.8000)+32;
                textView3.setText(String.valueOf(d));

            }
        });

    }
}
